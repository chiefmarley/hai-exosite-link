﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HAI_Exosite_link
{
    
    class Program
    {
        const string configFileName = "exosite_hai_link.cfg";
        static void Main(string[] args)
        {
            string[] configFile = new string[1]; 
            try
            {
                configFile = System.IO.File.ReadAllLines(configFileName);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(configFileName + " file not found");

            }
           

            string key1 = "";
            string key2 = "";

            string ip = "";
            string port = "";

            string cik = "";

            foreach (string line in configFile)
            {
                if (line.StartsWith("key1"))
                {
                    key1 = line.Split(' ')[1];
                }
                else if (line.StartsWith("key2"))
                {
                    key2 = line.Split(' ')[1];
                }
                else if (line.StartsWith("ip"))
                {
                    ip = line.Split(' ')[1];
                }
                else if (line.StartsWith("port"))
                {
                    port = line.Split(' ')[1];
                }
                else if (line.StartsWith("cik"))
                {
                    cik = line.Split(' ')[1];
                }
            }
            

            TimeSpan sleepTime = new TimeSpan(0, 0, 5); // time to sleep between event log checks

            // connect to controller, with unsolicited callback
            ControllerLink link = new ControllerLink(ip, port, key1, key2, true, sleepTime, cik);

        }
    }
}
