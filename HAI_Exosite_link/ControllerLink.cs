﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


using HAI_Shared;
using System.IO;

using clronep;

namespace HAI_Exosite_link
{
    class ControllerLink
    {
        private clsHAC HAC;

        string cik = "";

        DateTime lastRx = DateTime.Now;

        CountdownEvent latch = new CountdownEvent(1);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipaddress"> "192.158.0.1</param>
        /// <param name="port">4369</param>
        /// <param name="key1">encryption key 1</param>
        /// <param name="key2">Encryption key 2</param>
        /// <param name="getHistory">If true, will get all event history, else only
        /// processes new incoming messages.</param>
        public ControllerLink(string ipaddress, string port, string key1, string key2, bool getHistory, TimeSpan sleepTime, string l_cik)
        {
            HAC = new clsHAC();
            cik = l_cik;
            HAC.Connection.NetworkAddress = ipaddress;

            HAC.Connection.NetworkPort = Convert.ToUInt16(port);

            HAC.Connection.ControllerKey = clsUtil.HexString2ByteArray(String.Concat(key1, key2));

            HAC.PreferredNetworkProtocol = clsHAC.enuPreferredNetworkProtocol.TCP;

            HAC.Connection.ConnectionType = enuOmniLinkConnectionType.Network_TCP;

            HAC.Connection.Connect(HandleConnectStatus, HandleUnsolicitedPackets);
            int i = 0;

            while ( (HAC.Connection.ConnectionState != enuOmniLinkConnectionState.Online) && 
                    (HAC.Connection.ConnectionState != enuOmniLinkConnectionState.OnlineSecure))
            {
                Console.WriteLine(HAC.Connection.ConnectionState);
                Console.WriteLine(i++);
                Thread.Sleep(100);
            }

            // get names of objects
            getNamedObjects();

            // this
            //if (getHistory)
            //{
            //    clsHAC.enuConfigItems Items = clsHAC.enuConfigItems.Events;
            //    HAC.CommReadConfig(Items, ConfigStatusHandler);
                
            //}

            // enable sending of unsolicited
            HAC.Connection.Send(new clsOL2EnableNotifications(HAC.Connection, true), null);

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                //Console.WriteLine("Checking for new events...");
                clsOL2MsgRequestEventLogItem evtRequestMsg = new clsOL2MsgRequestEventLogItem(HAC.Connection,0,-1);
                HAC.Connection.Send(evtRequestMsg,handleEventLogRequest);
                // check event log

                // if we haven't received a message from the controller in 5 minutes, kill this app, supervisor
                // should start us back up.
                if ((DateTime.Now - lastRx) > new TimeSpan(0,5,0))
                {
                    HAC.Connection.Reconnect();
                    HAC.Connection.Connect(HandleConnectStatus, HandleUnsolicitedPackets);
                    i = 0;

                    while ((HAC.Connection.ConnectionState != enuOmniLinkConnectionState.Online) &&
                            (HAC.Connection.ConnectionState != enuOmniLinkConnectionState.OnlineSecure))
                    {
                        Console.WriteLine(HAC.Connection.ConnectionState);
                        Console.WriteLine(i++);
                        Thread.Sleep(100);
                    }

                    lastRx = DateTime.Now;
                }


            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
           
        }

        
        private void handleEventLogRequest(clsOmniLinkMessageQueueItem messageItem, byte[] response, bool timeout)
        {
            clsOL2MsgEventLogItem MSG = new clsOL2MsgEventLogItem(HAC.Connection, response);
            string res;
            
            if (HAC.EventLog.CopyProperties(MSG))
            {
                // we already have the event
            }
            else
            {
                res = HAC.EventLog[1].EventText();
                Console.WriteLine("New Event Found: " + res);
                Console.WriteLine("Adding new event to Exosite");
                // add to Exosite
                string alias_name = "controller_event";
                ClientOnepV1 conn = new ClientOnepV1("http://m2.exosite.com/api:v1/rpc/process", 3, cik);
                Result result;
                try
                {
                    result = conn.write(alias_name, res);
                    if (result.status == Result.OK)
                    {
                        Console.WriteLine("Successfully wrote value: " + res + ".\r\n");
                    }
                }
                catch (OnePlatformException e )
                {
                    Console.WriteLine("Write exception:");
                    Console.WriteLine(e.Message + "\r\n");
                }
                catch (HttpRPCRequestException e)
                {
                    Console.WriteLine("Exception while opening stream: ");
                    Console.WriteLine(e.Message + "\r\n");
                }
                catch (HttpRPCResponseException e)
                {
                    Console.WriteLine("Exception in Response: ");
                    Console.WriteLine(e.Message + "\r\n");
                }
                
            }
        }

        private void ConfigStatusHandler(clsHAC.enuConfigPhase Phase, int ix, ref bool Cancel)
        {
            // Display the status
            if (ix > 0)
            {
                Console.WriteLine(String.Format("Reading {0} ({1})",
                    Phase.ToString(),
                    ix));
            }
            else
            {
                Console.WriteLine("Reading " + Phase.ToString());
            }

            // we're done with config
            if (Phase == clsHAC.enuConfigPhase.Done)
            {
                // write out file if cb is set
                StreamWriter writer = new StreamWriter("output.txt");
                for (int i = 1; i < HAC.EventLog.Count; i++)
                {
                    writer.Write(HAC.EventLog[i].DateTimeText() + " ");
                    writer.WriteLine(HAC.EventLog[i].EventText());
                    //if (HAC.EventLog[i].Cmd == enuUnitCommand.Tripped)
                    //{
                    //    writer.Write(HAC.EventLog[i].DateTimeText() + " ");
                    //    writer.WriteLine("Zone " + HAC.Zones[HAC.EventLog[i].Pr2].Name + " tripped");

                    //}
                    //else if (HAC.EventLog[i].Cmd == enuUnitCommand.Security)
                    //{
                    //    writer.Write(HAC.EventLog[i].DateTimeText() + " ");
                    //    writer.WriteLine(HAC.Codes[HAC.EventLog[i].Par].Name + " DISARM " + HAC.Areas[HAC.EventLog[i].Pr2].Name);
                    //}
                    //else if (HAC.EventLog[i].Cmd == enuUnitCommand.SecurityAway)
                    //{
                    //    writer.Write(HAC.EventLog[i].DateTimeText() + " ");
                    //    writer.WriteLine(HAC.Codes[HAC.EventLog[i].Par].Name + " ARM AWAY: " + HAC.Areas[HAC.EventLog[i].Pr2].Name);
                    //}
                    //else
                    //{
                    //    writer.Write(HAC.EventLog[i].DateTimeText() + " ");
                    //    writer.WriteLine(HAC.EventLog[i].Cmd);
                    //}

                    //enuUnitCommand


                }
                writer.Close();
            }
        }

        private void HandleConnectStatus(enuOmniLinkCommStatus CS)
        {
            //if (InvokeRequired)
            //    /* CAUTION: In status handler routines you must use BeginInvoke
            //     * if you interact with any UI controls to prevent cross
            //     * threading errors.  It is a good practice to always use
            //     * BeginInvoke.
            //     * 
            //     * IMPORTANT: This is a notification which is usually fired off
            //     * in the middle of some other process.  You should not use
            //     * Invoke because it will block the initiating process which
            //     * can lead to errors caused by timing problems.
            //     */
            //    BeginInvoke(new HandleConnectStatusDelegate(HandleConnectStatus), new object[] { CS });
            //else
            //{
            
                switch (CS)
                {
                    case enuOmniLinkCommStatus.NoReply:
                        Console.WriteLine("CONNECTION STATUS: No Reply");
                        break;
                    case enuOmniLinkCommStatus.UnrecognizedReply:
                        Console.WriteLine("CONNECTION STATUS: Unrecognized Reply");
                        break;
                    case enuOmniLinkCommStatus.UnsupportedProtocol:
                        Console.WriteLine("CONNECTION STATUS: Unsupported Protocol");
                        break;
                    case enuOmniLinkCommStatus.ClientSessionTerminated:
                        Console.WriteLine("CONNECTION STATUS: Client Session Terminated");
                        break;
                    case enuOmniLinkCommStatus.ControllerSessionTerminated:
                        Console.WriteLine("CONNECTION STATUS: Controller Session Terminated");
                        break;
                    case enuOmniLinkCommStatus.CannotStartNewSession:
                        Console.WriteLine("CONNECTION STATUS: Cannot Start New Session");
                        break;
                    case enuOmniLinkCommStatus.LoginFailed:
                        Console.WriteLine("CONNECTION STATUS: Login Failed");
                        break;
                    case enuOmniLinkCommStatus.UnableToOpenSocket:
                        Console.WriteLine("CONNECTION STATUS: Unable To Open Socket");
                        break;
                    case enuOmniLinkCommStatus.UnableToConnect:
                        Console.WriteLine("CONNECTION STATUS: Unable To Connect");
                        break;
                    case enuOmniLinkCommStatus.SocketClosed:
                        Console.WriteLine("CONNECTION STATUS: Socket Closed");
                        break;
                    case enuOmniLinkCommStatus.UnexpectedError:
                        Console.WriteLine("CONNECTION STATUS: Unexpected Error");
                        break;
                    case enuOmniLinkCommStatus.UnableToCreateSocket:
                        Console.WriteLine("CONNECTION STATUS: Unable To Create Socket");
                        break;
                    case enuOmniLinkCommStatus.Retrying:
                        Console.WriteLine("CONNECTION STATUS: Retrying");
                        break;
                    case enuOmniLinkCommStatus.Connected:
                        // throw new Exception("Already connected... ");
                        //IdentifyController();
                        break;
                    case enuOmniLinkCommStatus.Connecting:
                        Console.WriteLine("CONNECTION STATUS: Connecting");
                        break;
                    case enuOmniLinkCommStatus.Disconnected:
                        Console.WriteLine("CONNECTION STATUS: Disconnected");
                        Console.WriteLine("Attempting to reconnect.");
                        HAC.Connection.Connect(HandleConnectStatus, HandleUnsolicitedPackets);
                        break;
                    case enuOmniLinkCommStatus.InterruptedFunctionCall:
                        Console.WriteLine("CONNECTION STATUS: Interrupted Function Call");
                        Console.WriteLine("attempting to reconnect");
                        // throw new Exception("function call interrupted");
                        Environment.Exit(3);


                        // This may work?  For now we'll kill ourselves and count on supervisord restarting us.
                        //HAC.Connection.Reconnect();
                        //HAC.Connection.Connect(HandleConnectStatus, HandleUnsolicitedPackets);
                        break;
                    case enuOmniLinkCommStatus.PermissionDenied:
                        Console.WriteLine("CONNECTION STATUS: Permission Denied");
                        break;
                    case enuOmniLinkCommStatus.BadAddress:
                        Console.WriteLine("CONNECTION STATUS: Bad Address");
                        break;
                    case enuOmniLinkCommStatus.InvalidArgument:
                        Console.WriteLine("CONNECTION STATUS: Invalid Argument");
                        break;
                    case enuOmniLinkCommStatus.TooManyOpenFiles:
                        Console.WriteLine("CONNECTION STATUS: Too Many Open Files");
                        break;
                    case enuOmniLinkCommStatus.ResourceTemporarilyUnavailable:
                        Console.WriteLine("CONNECTION STATUS: Resource Temporarily Unavailable");
                        break;
                    case enuOmniLinkCommStatus.OperationNowInProgress:
                        Console.WriteLine("CONNECTION STATUS: Operation Now In Progress");
                        break;
                    case enuOmniLinkCommStatus.OperationAlreadyInProgress:
                        Console.WriteLine("CONNECTION STATUS: Operation Already In Progress");
                        break;
                    case enuOmniLinkCommStatus.SocketOperationOnNonSocket:
                        Console.WriteLine("CONNECTION STATUS: Socket Operation On Non Socket");
                        break;
                    case enuOmniLinkCommStatus.DestinationAddressRequired:
                        Console.WriteLine("CONNECTION STATUS: Destination Address Required");
                        break;
                    case enuOmniLinkCommStatus.MessgeTooLong:
                        Console.WriteLine("CONNECTION STATUS: Message Too Long");
                        break;
                    case enuOmniLinkCommStatus.WrongProtocolType:
                        Console.WriteLine("CONNECTION STATUS: Wrong Protocol Type");
                        break;
                    case enuOmniLinkCommStatus.BadProtocolOption:
                        Console.WriteLine("CONNECTION STATUS: Bad Protocol Option");
                        break;
                    case enuOmniLinkCommStatus.ProtocolNotSupported:
                        Console.WriteLine("CONNECTION STATUS: Protocol Not Supported");
                        break;
                    case enuOmniLinkCommStatus.SocketTypeNotSupported:
                        Console.WriteLine("CONNECTION STATUS: Socket Type Not Supported");
                        break;
                    case enuOmniLinkCommStatus.OperationNotSupported:
                        Console.WriteLine("CONNECTION STATUS: Operation Not Supported");
                        break;
                    case enuOmniLinkCommStatus.ProtocolFamilyNotSupported:
                        Console.WriteLine("CONNECTION STATUS: Protocol Family Not Supported");
                        break;
                    case enuOmniLinkCommStatus.AddressFamilyNotSupported:
                        Console.WriteLine("CONNECTION STATUS: Address Family Not Supported");
                        break;
                    case enuOmniLinkCommStatus.AddressInUse:
                        Console.WriteLine("CONNECTION STATUS: Address In Use");
                        break;
                    case enuOmniLinkCommStatus.AddressNotAvailable:
                        Console.WriteLine("CONNECTION STATUS: Address Not Available");
                        break;
                    case enuOmniLinkCommStatus.NetworkIsDown:
                        Console.WriteLine("CONNECTION STATUS: Network Is Down");
                        break;
                    case enuOmniLinkCommStatus.NetworkIsUnreachable:
                        Console.WriteLine("CONNECTION STATUS: Network Is Unreachable");
                        break;
                    case enuOmniLinkCommStatus.NetworkReset:
                        Console.WriteLine("CONNECTION STATUS: Network Reset");
                        break;
                    case enuOmniLinkCommStatus.ConnectionAborted:
                        Console.WriteLine("CONNECTION STATUS: Connection Aborted");
                        break;
                    case enuOmniLinkCommStatus.ConnectionResetByPeer:
                        Console.WriteLine("CONNECTION STATUS: Connection Reset By Peer");
                        break;
                    case enuOmniLinkCommStatus.NoBufferSpaceAvailable:
                        Console.WriteLine("CONNECTION STATUS: No Buffer Space Available");
                        break;
                    case enuOmniLinkCommStatus.AlreadyConnected:
                        Console.WriteLine("CONNECTION STATUS: Already Connected");
                        break;
                    case enuOmniLinkCommStatus.NotConnected:
                        Console.WriteLine("CONNECTION STATUS: Not Connected");
                        break;
                    case enuOmniLinkCommStatus.CannotSendAfterShutdown:
                        Console.WriteLine("CONNECTION STATUS: Cannot Send After Shutdown");
                        break;
                    case enuOmniLinkCommStatus.ConnectionTimedOut:
                        Console.WriteLine("CONNECTION STATUS: Connection Timed Out");
                        break;
                    case enuOmniLinkCommStatus.ConnectionRefused:
                        Console.WriteLine("CONNECTION STATUS: Connection Refused");
                        break;
                    case enuOmniLinkCommStatus.HostIsDown:
                        Console.WriteLine("CONNECTION STATUS: Host Is Down");
                        break;
                    case enuOmniLinkCommStatus.HostUnreachable:
                        Console.WriteLine("CONNECTION STATUS: Host Unreachable");
                        break;
                    case enuOmniLinkCommStatus.TooManyProcesses:
                        Console.WriteLine("CONNECTION STATUS: Too Many Processes");
                        break;
                    case enuOmniLinkCommStatus.NetworkSubsystemIsUnavailable:
                        Console.WriteLine("CONNECTION STATUS: Network Subsystem Is Unavailable");
                        break;
                    case enuOmniLinkCommStatus.UnsupportedVersion:
                        Console.WriteLine("CONNECTION STATUS: Unsupported Version");
                        break;
                    case enuOmniLinkCommStatus.NotInitialized:
                        Console.WriteLine("CONNECTION STATUS: Not Initialized");
                        break;
                    case enuOmniLinkCommStatus.ShutdownInProgress:
                        Console.WriteLine("CONNECTION STATUS: Shutdown In Progress");
                        break;
                    case enuOmniLinkCommStatus.ClassTypeNotFound:
                        Console.WriteLine("CONNECTION STATUS: Class Type Not Found");
                        break;
                    case enuOmniLinkCommStatus.HostNotFound:
                        Console.WriteLine("CONNECTION STATUS: Host Not Found");
                        break;
                    case enuOmniLinkCommStatus.HostNotFoundTryAgain:
                        Console.WriteLine("CONNECTION STATUS: Host Not Found Try Again");
                        break;
                    case enuOmniLinkCommStatus.NonRecoverableError:
                        Console.WriteLine("CONNECTION STATUS: Non Recoverable Error");
                        break;
                    case enuOmniLinkCommStatus.NoDataOfRequestedType:
                        Console.WriteLine("CONNECTION STATUS: No Data Of Requested Type");
                        break;
                    default:
                        break;
                }
                //if ((HAC == null) ||
                //    (HAC.Connection.ConnectionState == enuOmniLinkConnectionState.Offline))
                //    SetOnLineStatus(false);
            //}
        }

        private bool HandleUnsolicitedPackets(byte[] B)
        {
        //    if (InvokeRequired)
        //        /* CAUTION: In status handler routines you must use BeginInvoke
        //         * if you interact with any UI controls to prevent cross
        //         * threading errors.  It is a good practice to always use
        //         * BeginInvoke.
        //         * 
        //         * IMPORTANT: This is a notification which is usually fired off
        //         * in the middle of some other process.  You should not use
        //         * Invoke because it will block the initiating process which
        //         * can lead to errors caused by timing problems.
        //         */
        //        BeginInvoke(new HandleUnsolicitedPacketDelegate(HandleUnsolicitedPackets), new object[] { B });
        //    else
        //    {
                // TODO: complete this routine

                lastRx = DateTime.Now;
                if ((B.Length > 3) && (B[0] == 0x21))
                {
                    // dummy handler just displays message type
                    Console.WriteLine("UNSOLICITED: " + ((enuOmniLink2MessageType)B[2]).ToString());

                    
                    switch ((enuOmniLink2MessageType)B[2])
                    {
                        case enuOmniLink2MessageType.ClearNames:
                            break;
                        case enuOmniLink2MessageType.DownloadNames:
                            break;
                        case enuOmniLink2MessageType.UploadNames:
                            break;
                        case enuOmniLink2MessageType.NameData:
                            break;
                        case enuOmniLink2MessageType.ClearVoices:
                            break;
                        case enuOmniLink2MessageType.DownloadVoices:
                            break;
                        case enuOmniLink2MessageType.UploadVoices:
                            break;
                        case enuOmniLink2MessageType.VoiceData:
                            break;
                        case enuOmniLink2MessageType.Command:
                            break;
                        case enuOmniLink2MessageType.EnableNotifications:
                            break;
                        case enuOmniLink2MessageType.SystemInformation:
                            break;
                        case enuOmniLink2MessageType.SystemStatus:
                            break;
                        case enuOmniLink2MessageType.SystemTroubles:
                            break;
                        case enuOmniLink2MessageType.SystemFeatures:
                            break;
                        case enuOmniLink2MessageType.Capacities:
                            break;
                        case enuOmniLink2MessageType.Properties:
                            break;
                        case enuOmniLink2MessageType.Status:
                            break;
                        case enuOmniLink2MessageType.EventLogItem:
                            clsOL2MsgEventLogItem evt = new clsOL2MsgEventLogItem(HAC.Connection, B);

                            break;
                        case enuOmniLink2MessageType.ValidateCode:
                            clsOL2MsgValidateCode vcd = new clsOL2MsgValidateCode(HAC.Connection, B);
                            break;
                        case enuOmniLink2MessageType.SystemFormats:
                            break;
                        case enuOmniLink2MessageType.Login:
                            break;
                        case enuOmniLink2MessageType.Logout:
                            break;
                        case enuOmniLink2MessageType.ActivateKeypadEmg:
                            break;
                        case enuOmniLink2MessageType.ExtSecurityStatus:
               
                            break;
                        case enuOmniLink2MessageType.CmdExtSecurity:
                  
                            break;
                        case enuOmniLink2MessageType.AudioSourceStatus:
                            break;
                        case enuOmniLink2MessageType.SystemEvents:

                            break;
                        case enuOmniLink2MessageType.ZoneReadyStatus:
                            break;
                        case enuOmniLink2MessageType.ExtendedStatus:
                            clsOL2MsgExtendedStatus ext_stat = new clsOL2MsgExtendedStatus(HAC.Connection, B);
                            string name = "";
                            if (B[3] == 0x01)
                            {
                                Console.WriteLine(DateTime.Now.ToString() + "Received extended zone msg");
                                name = HAC.Zones[B[5] * 256 + B[6]].Name;
                            }
                            else if (B[3] == 0x02)
                            {
                                Console.WriteLine(DateTime.Now.ToString() + "Received extended unit msg");
                                name = HAC.Units[B[5] * 256 + B[6]].Name;
                            }
                            else
                            {
                                Console.WriteLine(DateTime.Now.ToString() + "Received undetermined zone msg: " + B[3].ToString());
                            }

                            if (name != "")
                            {
                                var currentStatus = B[7] & 3;
                                string statusStr = "";
                                if (currentStatus == 0)
                                {
                                    statusStr = "SECURE";
                                }
                                else if (currentStatus == 1)
                                {
                                    statusStr = "NOT READY";
                                }
                                else if (currentStatus == 2)
                                {
                                    statusStr = "TROUBLE";
                                }


                                string res = name + " is " + statusStr;

                                Console.WriteLine("New unsolicited event Found: " + res);
                                Console.WriteLine("Adding new event to Exosite");
                                // add to Exosite
                                string alias_name = "controller_event";
                                ClientOnepV1 conn = new ClientOnepV1("http://m2.exosite.com/api:v1/rpc/process", 3, cik);
                                Result result;
                                try
                                {
                                    result = conn.write(alias_name, res);
                                    if (result.status == Result.OK)
                                    {
                                        Console.WriteLine("Successfully wrote value: " + res + ".\r\n");
                                    }
                                }
                                catch (OnePlatformException e)
                                {
                                    Console.WriteLine("Write exception:");
                                    Console.WriteLine(e.Message + "\r\n");
                                }
                                catch (HttpRPCRequestException e)
                                {
                                    Console.WriteLine("Exception while opening stream: ");
                                    Console.WriteLine(e.Message + "\r\n");
                                }
                                catch (HttpRPCResponseException e)
                                {
                                    Console.WriteLine("Exception in Response: ");
                                    Console.WriteLine(e.Message + "\r\n");
                                }

                            }

                            break;
                        default:
                            break;
                    }
                    
                }
            //}
            return true;
        }


        AutoResetEvent waitForNameHandle = new AutoResetEvent(false);
        private void getNamedObjects()
        {
            currentNameRequestType = enuObjectType.Area;
            Console.Write("Fetching Area names from controller...");
            GetNames(0);
            waitForNameHandle.WaitOne();
            Console.WriteLine("Done");

            Console.Write("Fetching Unit names from controller...");
            currentNameRequestType = enuObjectType.Unit;
            GetNames(0);
            waitForNameHandle.WaitOne();
            Console.WriteLine("Done");


            currentNameRequestType = enuObjectType.Zone;

            Console.Write("Fetching Zone names from controller...");
            GetNames(0);
            waitForNameHandle.WaitOne();
            Console.WriteLine("Done");

            //currentNameRequestType = enuObjectType.Console;
            //Console.Write("Fetching Console names from controller...");
            //GetNames(0);
            //waitForNameHandle.WaitOne();
            //Console.WriteLine("Done");

            currentNameRequestType = enuObjectType.Code;
            Console.Write("Fetching Code names from controller...");
            GetNames(0);
            waitForNameHandle.WaitOne();
            Console.WriteLine("Done");
        }

        private enuObjectType currentNameRequestType;
        private void GetNames(int ix)
        {
            clsOL2MsgRequestProperties MSG = new clsOL2MsgRequestProperties(HAC.Connection);
            MSG.ObjectType = currentNameRequestType;
            MSG.IndexNumber = (UInt16)ix;
            MSG.RelativeDirection = 1;  // next object after IndexNumber
            MSG.Filter1 = 1;  // (0=Named or Unnamed, 1=Named, 2=Unnamed).
            MSG.Filter2 = 0;  // Any Area
            MSG.Filter3 = 0;  // Any Room
            HAC.Connection.Send(MSG, HandleNameResponsePropertiesResponseAndGetNext);
        }

        private void HandleNameResponsePropertiesResponseAndGetNext(clsOmniLinkMessageQueueItem M,
           byte[] B, bool Timeout)
        {
            if (Timeout)
                return;
            //if (InvokeRequired)
            //    /* CAUTION: In message handler routines you must use Invoke
            //     * if you interact with any UI controls to prevent cross
            //     * threading errors.  It is a good practice to always use
            //     * Invoke.
            //     * IMPORTANT: DO NOT USE BeginInvoke as this will cause the
            //     * handler to be called asynchronously, and the triggering
            //     * message may no longer be the first message in the queue.
            //     * This can lead to some unpredictable behavior.
            //     */
            //    Invoke(new HandleReceivedPacketDelegate(HandleAreaPropertiesResponseAndGetNext),
            //        new object[] { M, B, Timeout });
            else
            {
                // does it look like a valid response
                if ((B.Length > 3) && (B[0] == 0x21))
                {
                    switch ((enuOmniLink2MessageType)B[2])
                    {
                        case enuOmniLink2MessageType.EOD:
                            //// if the list is not empty, select first item
                            //if (CB.Items.Count > 0)
                            //    CB.SelectedIndex = 0;
                            // Close();  // End of data, we are done...
                            waitForNameHandle.Set();
                            break;
                        case enuOmniLink2MessageType.Properties:
                            // create a new message from byte array for ease of processing
                            clsOL2MsgProperties MSG = new clsOL2MsgProperties(
                                HAC.Connection, B);
                            // copy the unit properties into the unit list
                            switch (currentNameRequestType)
                            {
                                case enuObjectType.Area:
                                    {
                                        HAC.Areas.CopyProperties(MSG);
                                        break;
                                    }
                                case enuObjectType.Code:
                                    {
                                        HAC.Codes.CopyProperties(MSG);
                                        break;
                                    }
                                case enuObjectType.Console:
                                    {
                                        HAC.Consoles.CopyProperties(MSG);
                                        break;
                                    }
                                case enuObjectType.Zone:
                                    {
                                        HAC.Zones.CopyProperties(MSG);
                                        break;
                                    }
                            }
                            // get a pointer the the unit that was just updated
                            //clsArea Unt = HAC.Areas[MSG.ObjectNumber];
                            // display in progress label

                            // get next named unit
                            GetNames(MSG.ObjectNumber);
                            break;
                        default:
                            // handle unexpected response
                            break;
                    }
                }
                // else handle unexpected response
            }
        }

    }
}

