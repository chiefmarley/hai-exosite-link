# HAI Exosite Link
The HAI Exosite link program demonstrates the ability of connecting an [HAI Omni
Pro II](http://www.homeauto.com/Products/HAISystems/OmniPro2/omnipro2_tech.asp) 
to the [Exosite One Platform](http://exosite.com/products/onep).  Although HAI
Exosite Link is written in C#, all of the calls are compatible with the [Mono
Project](http://www.mono-project.com/Main_Page), and has been tested to run a
Raspberry Pi running Rasbian Wheezy.

# How to
Follow these steps for the quickest way to get the your HAI system connected to
Exosite's cloud.

## Setup Exosite Link

 1. Create an account on chiefmarley.exosite.com
 2. Add an HAI_Omni device to your account
 	* Copy your cik to a file.  You will need it during the next step

## Setup Hardware

 1. Obtain a [Raspberry Pi](https://www.amzn.com/dp/B009SQQF9C/?tag=hxolink-20)
    and an [SD card](https://www.amazon.com/dp/B003VNKNEG/?tag=hxolink-20)
 2. Use the [this](http://elinux.org/            RPi_Easy_SD_Card_Setup#Using_Windows_7_or_Windows_XP) 
    guide for installing Rasbian on your Raspberry Pi
 3. From the the command line of your Raspberry Pi, run the following commands
    1. `sudo apt-get update`
    2. `sudo apt-get install mono-complete` 	
 4. Download and extract the HAI Exosite Link Program. From the command prompt:
 	1.  `cd /home/pi/`
 	2.  `wget https://bitbucket.org/chiefmarley/hai-exosite-link/downloads/HAI_Exosite_link.zip`
 	3.  `unzip HAI_Exosite_link.zip`
 5. Edit config file with your settings
 6. Run the HAI_Exosite_link.exe
    1. `mono HAI_Exosite_link.exe`
 
 The above method will start the application and you will be able to see
 communications begin with your HAI system.  However, as soon as you close the
 terminal window, the application will exit.  To allow the HAI_Exosite_link to 
 start when your raspberry pi starts up and run in the background, we will use
 a program called supervisord.  Follow the following steps to install supervisord
 and configure it to run HAI_Exosite_link in the background.
 
  1. Install supervisord
    1. `sudo apt-get install supervisord`
  2. Add a file to ... with the following contents
  
    [program:hai_exosite_link]
    command=python -u /path/to/HAI_Exosite_link.exe
    directory=/root/applications
    stdout_logfile=/root/applications/HAI_Exosite_link.log
    redirect_stderr=true
    stdout_logfile_maxbytes=1MB
    stdout_logfile_backups=4

